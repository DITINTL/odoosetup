**Install command list for setting up Odoo 10 on Ubuntu 18.04.3**
Launch a VPS on your favourite Cloud provider, log in as sudo or root user and type the following commands to install Odoo 10.


	sudo apt-get update
	sudo apt-get upgrade
	sudo apt-get install python-pip

	pip install python-pypdf

	sudo apt-get install python-dateutil 
	sudo apt-get install python-docutils 
	sudo apt-get install python-feedparser 
	sudo apt-get install python-jinja2 
	sudo apt-get install python-ldap 
	sudo apt-get install python-libxslt1 
	sudo apt-get install python-lxml 
	sudo apt-get install python-mako 
	sudo apt-get install python-mock 
	sudo apt-get install python-openid 
	sudo apt-get install python-psycopg2 
	sudo apt-get install python-psutil 

	alias si='sudo apt-get install'

	si python-babel
	si python-pychart 
	si python-pydot 
	si python-pyparsing 
	si python-reportlab 
	si python-simplejson 
	si python-tz
	si python-unittest2
	si python-vatnumber 
	si python-vobject 
	si python-webdav 
	si python-werkzeug 
	si python-xlwt 
	si python-yaml 
	si python-zsi
	si poppler-utils
	pip install pypdf
	si python-pdf	
	si python-passlib 
	si python-decorator
	si gcc
	si python-dev 
	si mc 
	si bzr
	si python-setuptools
	si python-markupsafe 
	si python-reportlab-accel 
	si python-zsi 
	si python-yaml 



	si python-argparse
	si python-openssl 
	si python-egenix-mxdatetime
	si python-usb 
	si python-serial 
	si lptools
	si make 
	si python-pydot
	si python-psutil 
	si python-paramiko 
	si poppler-utils
	si python-pdftools
	si antiword
	si python-requests 
	si python-xlsxwriter 
	si python-suds 
	si python-psycogreen 
	si python-ofxparse 
	si python-gevent

	sudo apt-get install -y npm

	sudo npm install -g less less-plugin-clean-css

*Open file using command*

	sudo nano /etc/apt/sources.list.d/pgdg.list

*Add this line :*

	deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main


*save and exit using commands*


	ctrl o
	ctrl x

	wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
	sudo apt-get update
	sudo apt-get install postgresql-9.6


	sudo su postgres
	cd
	createuser -s odoo


	createuser -s ubuntu_user_name
	exit

	sudo adduser --system --home=/opt/odoo --group odoo
	cd /opt/odoo
	sudo wget https://pypi.python.org/packages/a8/70/bd554151443fe9e89d9a934a7891aaffc63b9cb5c7d608972919a002c03c/gdata-2.0.18.tar.gz
	sudo tar zxvf gdata-2.0.18.tar.gz
	sudo chown -R odoo: gdata-2.0.18


	sudo -s

	cd gdata-2.0.18/

	python setup.py install

	exit




	cd /opt/odoo

	sudo wget https://github.com/odoo/odoo/archive/10.0.zip

	sudo unzip 10.0.zip

	sudo chown -R odoo: odoo-10.0

	sudo chown -R odoo: odoo-10.0

	sudo mkdir /var/log/odoo

	sudo chown -R odoo:root /var/log/odoo

	sudo cp /opt/odoo/odoo-10.0/debian/odoo.conf /etc/odoo.conf

	sudo chown odoo: /etc/odoo.conf

	sudo gedit /etc/odoo.conf —> use nano instead of gedit


*Content should look like this:*
	
	[options]
	; This is the password that allows database operations:
	admin_passwd = admin
	db_host = False
	db_port = False
	db_user = odoo
	db_password = False
	addons_path = /usr/lib/python2.7/dist-packages/odoo/addons

	;Log Settings

	logfile = /var/log/odoo/odoo.log

	log_level = error
	
*Save and exit using the comands*

	ctrl o + enter
	ctrl x

	sudo wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.1/wkhtmltox-0.12.1_linux-trusty-amd64.deb
	sudo dpkg -i  wkhtmltox-0.12.1_linux-trusty-amd64.deb
	sudo cp /usr/local/bin/wkhtmltoimage /usr/bin/wkhtmltoimage
	sudo cp /usr/local/bin/wkhtmltopdf /usr/bin/wkhtmltopdf

	cd /opt/odoo/odoo-10.0
	./odoo-bin	

*Visit server IP address on given Odoo port. Typically 8069.*		
